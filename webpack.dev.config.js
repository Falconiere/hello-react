const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const WebpackRTLPlugin = require('webpack-rtl-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const Dotenv = require('dotenv-webpack');

/**
 * Imports by sass-loader
 */
var importsSass =  '@import "_variables.scss"; @import "_mixins.scss";';

module.exports = {
   //devtool: 'source-map',
    entry: __dirname+'/app/resources/config/app.tsx',
    output: {
      path: path.join(__dirname, './app/public/js/'),
      filename: 'app.js'
    },
    watch: true,
    resolve: {
      extensions: ['.js', '.json', '.ts', '.tsx','.jsx'],
    },
    module: {
      rules: [
        {
          test: /\.(ts|tsx)?$/,
          loader: "awesome-typescript-loader",
          exclude: /node_modules/
        },
        {
          test: /\.(js|jsx)$/,
          use: 'babel-loader',
          exclude: [
            /(node_modules|bower_components)/,
            path.resolve(__dirname, 'app/server.js'),
          ]
        },
        {
          test: /\.(css|scss)$/,
          exclude: ["node_modules"],
          // sourceMap: true,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use:[
            {
              loader: 'css-loader',
              options: {
                minimize: true || {/* CSSNano Options */},
                sourceMap:true,
                url:false
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap:true,
              plugins: (loader) => [
                //require('rtl-css'),
                require('postcss-smart-import'),
                require('autoprefixer'),
              ]
            }
          },
          {
            loader:"sass-loader",
            options: {
              sourceMap:true,
              data:importsSass,
              includePaths: [path.resolve(__dirname, "./app/resources/assets/scss/")],
              outputStyle: 'compressed',
            }

          },
          {
            loader:'import-glob'
          },
        ]
        })
      }
      ]
    },
   
    plugins:  [
     //  new Dotenv({
     //   path: './.env', // Path to .env file (this is the default)
     //   safe: true // load .env.example (defaults to "false" which does not use dotenv-safe)
     // }),
      // new webpack.DefinePlugin({
      //   "env":{
      //     "BASE_API": process.env.BASE_API
      //   }
      // }),
      new ExtractTextPlugin("../css/app.css", {
        allChunks: true
      })
    // new webpack.LoaderOptionsPlugin({
    //   minimize: true,
    // }),
    // new UglifyJSPlugin()
  ]
    
}