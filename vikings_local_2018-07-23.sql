# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.3.7-MariaDB)
# Database: vikings_local
# Generation Time: 2018-07-23 04:44:34 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table adonis_schema
# ------------------------------------------------------------

DROP TABLE IF EXISTS `adonis_schema`;

CREATE TABLE `adonis_schema` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `batch` int(11) DEFAULT NULL,
  `migration_time` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `adonis_schema` WRITE;
/*!40000 ALTER TABLE `adonis_schema` DISABLE KEYS */;

INSERT INTO `adonis_schema` (`id`, `name`, `batch`, `migration_time`)
VALUES
	(1,'1420276362470_beer_type_schema',1,'2018-05-04 01:22:22'),
	(2,'1420276679063_beer_color_schema',1,'2018-05-04 01:22:22'),
	(3,'1420280056768_file_schema',1,'2018-05-04 01:22:22'),
	(4,'1420281389988_country_schema',1,'2018-05-04 01:22:22'),
	(5,'1420283828709_city_schema',1,'2018-05-04 01:22:22'),
	(6,'1420287895998_place_schema',1,'2018-05-04 01:22:22'),
	(7,'1420291910911_beer_schema',1,'2018-05-04 01:22:23'),
	(8,'1420700034279_user',1,'2018-05-04 01:22:23'),
	(9,'1420808026619_gallery_schema',1,'2018-05-04 01:22:23'),
	(10,'1420809874070_file_gallery_schema',1,'2018-05-04 01:22:23'),
	(11,'1503250034280_token',1,'2018-05-04 01:22:23'),
	(12,'1519844411476_role_schema',1,'2018-05-04 01:22:23'),
	(13,'1519844884764_permission_schema',1,'2018-05-04 01:22:23'),
	(14,'1519845293258_permission_role_schema',1,'2018-05-04 01:22:23'),
	(15,'1520213139081_role_user_schema',1,'2018-05-04 01:22:23'),
	(16,'1520772861250_award_schema',1,'2018-05-04 01:22:23'),
	(17,'1520774956034_event_schema',1,'2018-05-04 01:22:23'),
	(18,'1520777753728_beer_event_schema',1,'2018-05-04 01:22:23'),
	(19,'1520780677184_place_user_schema',1,'2018-05-04 01:22:23'),
	(20,'1520811489823_evaluation_schema',1,'2018-05-04 01:22:23'),
	(21,'1520814172651_save_schema',1,'2018-05-04 01:22:23'),
	(22,'1521046286577_invitation_schema',1,'2018-05-04 01:22:23'),
	(23,'1521507473351_friend_request_schema',1,'2018-05-04 01:22:23'),
	(24,'1521912261082_duel_schema',1,'2018-05-04 01:22:23'),
	(25,'1521983370059_award_beer_schema',1,'2018-05-04 01:22:23'),
	(26,'1521983514997_beer_place_schema',1,'2018-05-04 01:22:23'),
	(27,'1522116003837_beers_schema',1,'2018-05-04 01:22:24'),
	(28,'1522505053778_duel_vote_schema',1,'2018-05-04 01:22:24'),
	(29,'1522678334234_like_schema',1,'2018-05-04 01:22:24'),
	(30,'1523224055719_users_schema',1,'2018-05-04 01:22:24'),
	(31,'1528587159304_notifications_schema',2,'2018-06-17 16:59:33'),
	(32,'1528682311660_devices_schema',2,'2018-06-17 16:59:33'),
	(34,'1532299360890_messages_schema',3,'2018-07-23 00:56:06');

/*!40000 ALTER TABLE `adonis_schema` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table award_beer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `award_beer`;

CREATE TABLE `award_beer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `beer_id` int(10) unsigned DEFAULT NULL,
  `award_id` int(10) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `award_beer_beer_id_index` (`beer_id`),
  KEY `award_beer_award_id_index` (`award_id`),
  CONSTRAINT `award_beer_award_id_foreign` FOREIGN KEY (`award_id`) REFERENCES `awards` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `award_beer_beer_id_foreign` FOREIGN KEY (`beer_id`) REFERENCES `beers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `award_beer` WRITE;
/*!40000 ALTER TABLE `award_beer` DISABLE KEYS */;

INSERT INTO `award_beer` (`id`, `beer_id`, `award_id`, `created_at`, `updated_at`)
VALUES
	(1,54,1,NULL,NULL),
	(3,56,1,NULL,NULL),
	(13,55,1,NULL,NULL),
	(14,57,1,NULL,NULL);

/*!40000 ALTER TABLE `award_beer` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table awards
# ------------------------------------------------------------

DROP TABLE IF EXISTS `awards`;

CREATE TABLE `awards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `order` int(10) unsigned DEFAULT 0,
  `visible` tinyint(1) DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `awards` WRITE;
/*!40000 ALTER TABLE `awards` DISABLE KEYS */;

INSERT INTO `awards` (`id`, `name`, `order`, `visible`, `created_at`, `updated_at`)
VALUES
	(1,'premio 01',0,1,'2018-05-06 21:10:22','2018-05-06 21:10:22');

/*!40000 ALTER TABLE `awards` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table beer_colors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `beer_colors`;

CREATE TABLE `beer_colors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `hex` varchar(191) NOT NULL,
  `order` int(10) unsigned DEFAULT 0,
  `visible` tinyint(1) DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `beer_colors` WRITE;
/*!40000 ALTER TABLE `beer_colors` DISABLE KEYS */;

INSERT INTO `beer_colors` (`id`, `name`, `hex`, `order`, `visible`, `created_at`, `updated_at`)
VALUES
	(1,'Wecfilo.','#40af97',0,1,'2018-05-04 01:44:52','2018-05-04 01:44:52'),
	(2,'Fujcidamu.','#9E3535',0,1,'2018-05-04 01:44:52','2018-05-04 04:16:10');

/*!40000 ALTER TABLE `beer_colors` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table beer_event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `beer_event`;

CREATE TABLE `beer_event` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `beer_id` int(10) unsigned DEFAULT NULL,
  `event_id` int(10) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `beer_event_beer_id_index` (`beer_id`),
  KEY `beer_event_event_id_index` (`event_id`),
  CONSTRAINT `beer_event_beer_id_foreign` FOREIGN KEY (`beer_id`) REFERENCES `beers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `beer_event_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `beer_event` WRITE;
/*!40000 ALTER TABLE `beer_event` DISABLE KEYS */;

INSERT INTO `beer_event` (`id`, `beer_id`, `event_id`, `created_at`, `updated_at`)
VALUES
	(11,54,1,NULL,NULL),
	(12,55,1,NULL,NULL),
	(13,57,1,NULL,NULL);

/*!40000 ALTER TABLE `beer_event` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table beer_place
# ------------------------------------------------------------

DROP TABLE IF EXISTS `beer_place`;

CREATE TABLE `beer_place` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `beer_id` int(10) unsigned DEFAULT NULL,
  `place_id` int(10) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `beer_place_beer_id_index` (`beer_id`),
  KEY `beer_place_place_id_index` (`place_id`),
  CONSTRAINT `beer_place_beer_id_foreign` FOREIGN KEY (`beer_id`) REFERENCES `beers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `beer_place_place_id_foreign` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `beer_place` WRITE;
/*!40000 ALTER TABLE `beer_place` DISABLE KEYS */;

INSERT INTO `beer_place` (`id`, `beer_id`, `place_id`, `created_at`, `updated_at`)
VALUES
	(125,56,1,NULL,NULL),
	(126,55,1,NULL,NULL),
	(127,54,2,NULL,NULL),
	(128,55,2,NULL,NULL),
	(129,56,3,NULL,NULL),
	(130,55,3,NULL,NULL),
	(131,56,4,NULL,NULL),
	(132,55,4,NULL,NULL),
	(133,56,10,NULL,NULL),
	(134,55,10,NULL,NULL);

/*!40000 ALTER TABLE `beer_place` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table beer_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `beer_types`;

CREATE TABLE `beer_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `order` int(10) unsigned DEFAULT 0,
  `visible` tinyint(1) DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `beer_types` WRITE;
/*!40000 ALTER TABLE `beer_types` DISABLE KEYS */;

INSERT INTO `beer_types` (`id`, `name`, `order`, `visible`, `created_at`, `updated_at`)
VALUES
	(11,'Weiss',0,1,'2018-05-04 18:55:09','2018-05-04 18:55:09');

/*!40000 ALTER TABLE `beer_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table beers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `beers`;

CREATE TABLE `beers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `ideal_cup` varchar(191) DEFAULT NULL,
  `ibu` varchar(191) DEFAULT NULL,
  `alcohol_content` varchar(191) DEFAULT NULL,
  `volume` varchar(191) DEFAULT NULL,
  `ideal_tempeture` varchar(191) DEFAULT NULL,
  `side_dish` varchar(191) DEFAULT NULL,
  `beer_sommelier` varchar(191) DEFAULT NULL,
  `profile_pic` int(10) unsigned DEFAULT NULL,
  `beer_type_id` int(10) unsigned NOT NULL,
  `beer_color_id` int(10) unsigned NOT NULL,
  `country_id` int(10) unsigned NOT NULL,
  `visible` tinyint(1) DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `ingredients` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `beers_beer_type_id_index` (`beer_type_id`),
  KEY `beers_beer_color_id_index` (`beer_color_id`),
  KEY `beers_country_id_index` (`country_id`),
  KEY `beers_profile_pic_foreign` (`profile_pic`),
  CONSTRAINT `beers_beer_color_id_foreign` FOREIGN KEY (`beer_color_id`) REFERENCES `beer_colors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `beers_beer_type_id_foreign` FOREIGN KEY (`beer_type_id`) REFERENCES `beer_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `beers_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `beers_profile_pic_foreign` FOREIGN KEY (`profile_pic`) REFERENCES `files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `beers` WRITE;
/*!40000 ALTER TABLE `beers` DISABLE KEYS */;

INSERT INTO `beers` (`id`, `name`, `ideal_cup`, `ibu`, `alcohol_content`, `volume`, `ideal_tempeture`, `side_dish`, `beer_sommelier`, `profile_pic`, `beer_type_id`, `beer_color_id`, `country_id`, `visible`, `created_at`, `updated_at`, `ingredients`)
VALUES
	(54,'Cerveja 01','taça','3%','3%','500ml','10 graus','Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.','Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. ',13,11,1,1,1,'2018-05-06 21:37:57','2018-05-06 21:37:57','asasasas,asasas,assasasas,asasasasasasa,as,asasasa'),
	(55,'Cerveja 03','taçå','3%','3%','1l','10 graus','Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.','Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.',13,11,1,2,1,'2018-05-06 21:40:33','2018-05-07 00:20:13','Lorem Ipsum,Lorem Ips,Lorem '),
	(56,'Cerveja 02','taçå','3%','3%','1l','10 graus','Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.','Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.',13,11,1,2,1,'2018-05-06 21:40:35','2018-05-06 21:40:35','Lorem Ipsum,Lorem Ips,Lorem '),
	(57,'Cerveja 04','taçå','3%','3%','1l','10 graus','Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.','Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.',13,11,1,2,1,'2018-05-06 21:40:36','2018-05-14 23:13:17','Lorem Ipsum,Lorem Ips,Lorem ');

/*!40000 ALTER TABLE `beers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cities`;

CREATE TABLE `cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `order` int(10) unsigned DEFAULT 0,
  `country_id` int(10) unsigned DEFAULT NULL,
  `visible` tinyint(1) DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cities_country_id_foreign` (`country_id`),
  CONSTRAINT `cities_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;

INSERT INTO `cities` (`id`, `name`, `order`, `country_id`, `visible`, `created_at`, `updated_at`)
VALUES
	(1,'Buenos Aires',0,1,1,'2018-05-04 01:44:51','2018-05-04 01:44:51'),
	(2,'Mendoza',0,1,1,'2018-05-04 01:44:51','2018-05-04 01:44:51'),
	(3,'Brasilia',0,2,1,'2018-05-04 01:44:51','2018-05-04 01:44:51'),
	(4,'Rio de janeiro',0,2,1,'2018-05-04 01:44:51','2018-05-04 01:44:51'),
	(5,'Santiago',0,3,1,'2018-05-04 01:44:51','2018-05-04 01:44:51'),
	(6,'Montevideo',0,4,1,'2018-05-04 01:44:51','2018-05-04 01:44:51'),
	(7,'Caracas',0,5,1,'2018-05-04 01:44:51','2018-05-04 01:44:51'),
	(8,'Porlamar',0,5,1,'2018-05-04 01:44:51','2018-05-04 01:44:51');

/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table countries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `order` int(10) unsigned DEFAULT 0,
  `visible` tinyint(1) DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;

INSERT INTO `countries` (`id`, `name`, `order`, `visible`, `created_at`, `updated_at`)
VALUES
	(1,'Argentina',0,1,'2018-05-04 01:44:51','2018-05-04 01:44:51'),
	(2,'Brazil',1,1,'2018-05-04 01:44:51','2018-05-04 01:44:51'),
	(3,'Chile',2,1,'2018-05-04 01:44:51','2018-05-04 01:44:51'),
	(4,'Uruguay',3,1,'2018-05-04 01:44:51','2018-05-04 01:44:51'),
	(5,'Venezuela',4,1,'2018-05-04 01:44:51','2018-05-04 01:44:51');

/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table devices
# ------------------------------------------------------------

DROP TABLE IF EXISTS `devices`;

CREATE TABLE `devices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `token` longtext DEFAULT NULL,
  `platform` enum('ios','android') DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `devices_user_id_index` (`user_id`),
  CONSTRAINT `devices_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `devices` WRITE;
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;

INSERT INTO `devices` (`id`, `token`, `platform`, `user_id`, `created_at`, `updated_at`)
VALUES
	(1,'64dd732cac5ecd1cf5aa8d39d5d102ba8d8dbe51036111673a1796a92478a24d','ios',8,'2018-06-17 19:08:39','2018-06-17 19:08:39'),
	(4,'64dd732cac5ecd1cf5aa8d39d5d102ba8d8dbe51036111673a1796a92478a24d','ios',8,'2018-06-17 20:03:18','2018-06-17 20:03:18');

/*!40000 ALTER TABLE `devices` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table duel_votes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `duel_votes`;

CREATE TABLE `duel_votes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `beer_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `duel_id` int(10) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `duel_votes_user_id_duel_id_unique` (`user_id`,`duel_id`),
  KEY `duel_votes_beer_id_index` (`beer_id`),
  KEY `duel_votes_user_id_index` (`user_id`),
  KEY `duel_votes_duel_id_index` (`duel_id`),
  CONSTRAINT `duel_votes_beer_id_foreign` FOREIGN KEY (`beer_id`) REFERENCES `beers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `duel_votes_duel_id_foreign` FOREIGN KEY (`duel_id`) REFERENCES `duels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `duel_votes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table duels
# ------------------------------------------------------------

DROP TABLE IF EXISTS `duels`;

CREATE TABLE `duels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `beer_left_id` int(10) unsigned NOT NULL,
  `beer_right_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` enum('DRAFT','PROCESS','FINISHED') NOT NULL DEFAULT 'DRAFT',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `duels_beer_left_id_index` (`beer_left_id`),
  KEY `duels_beer_right_id_index` (`beer_right_id`),
  KEY `duels_user_id_index` (`user_id`),
  CONSTRAINT `duels_beer_left_id_foreign` FOREIGN KEY (`beer_left_id`) REFERENCES `beers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `duels_beer_right_id_foreign` FOREIGN KEY (`beer_right_id`) REFERENCES `beers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `duels_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `duels` WRITE;
/*!40000 ALTER TABLE `duels` DISABLE KEYS */;

INSERT INTO `duels` (`id`, `beer_left_id`, `beer_right_id`, `user_id`, `status`, `created_at`, `updated_at`)
VALUES
	(1,54,55,8,'PROCESS','2018-06-03 08:59:47','2018-06-03 08:59:47'),
	(2,55,57,8,'PROCESS','2018-06-03 09:00:12','2018-06-03 09:00:12'),
	(3,55,54,8,'PROCESS','2018-06-03 09:05:53','2018-06-03 09:05:53'),
	(4,54,56,8,'PROCESS','2018-06-30 21:31:07','2018-06-30 21:31:07'),
	(5,54,56,8,'PROCESS','2018-07-13 21:58:12','2018-07-13 21:58:12');

/*!40000 ALTER TABLE `duels` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table evaluations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `evaluations`;

CREATE TABLE `evaluations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `place_id` int(10) unsigned DEFAULT NULL,
  `event_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `description` text DEFAULT NULL,
  `type` enum('place','event') NOT NULL,
  `rate` int(10) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `evaluations_user_id_index` (`user_id`),
  KEY `evaluations_rate_index` (`rate`),
  KEY `evaluations_event_id_foreign` (`event_id`),
  KEY `evaluations_place_id_foreign` (`place_id`),
  CONSTRAINT `evaluations_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `evaluations_place_id_foreign` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `evaluations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table events
# ------------------------------------------------------------

DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `phone` varchar(191) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `city_id` int(10) unsigned NOT NULL,
  `address` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `cover_pic` int(10) unsigned DEFAULT NULL,
  `gallery_id` int(10) unsigned DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `events_city_id_index` (`city_id`),
  KEY `events_gallery_id_index` (`gallery_id`),
  KEY `events_created_by_index` (`created_by`),
  KEY `events_cover_pic_foreign` (`cover_pic`),
  CONSTRAINT `events_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `events_cover_pic_foreign` FOREIGN KEY (`cover_pic`) REFERENCES `files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `events_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `events_gallery_id_foreign` FOREIGN KEY (`gallery_id`) REFERENCES `galleries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;

INSERT INTO `events` (`id`, `name`, `phone`, `start_date`, `end_date`, `start_time`, `end_time`, `city_id`, `address`, `description`, `cover_pic`, `gallery_id`, `created_by`, `created_at`, `updated_at`)
VALUES
	(1,'Evento 01','09090909','2018-05-03','2018-05-04','11:11:00','11:11:00',2,'Barra da Tijuca - Rio de Janeiro - RJ','Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. ',29,NULL,1,'2018-05-06 19:39:32','2018-05-16 02:13:43');

/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table file_gallery
# ------------------------------------------------------------

DROP TABLE IF EXISTS `file_gallery`;

CREATE TABLE `file_gallery` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_id` int(10) unsigned NOT NULL,
  `gallery_id` int(10) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `file_gallery_file_id_index` (`file_id`),
  KEY `file_gallery_gallery_id_index` (`gallery_id`),
  CONSTRAINT `file_gallery_file_id_foreign` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `file_gallery_gallery_id_foreign` FOREIGN KEY (`gallery_id`) REFERENCES `galleries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table files
# ------------------------------------------------------------

DROP TABLE IF EXISTS `files`;

CREATE TABLE `files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `path` varchar(10) NOT NULL,
  `type` enum('image','video','document','audio') NOT NULL,
  `size` enum('original','small','medium','full') NOT NULL DEFAULT 'original',
  `user_id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `files_user_id_index` (`user_id`),
  KEY `files_parent_id_foreign` (`parent_id`),
  CONSTRAINT `files_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;

INSERT INTO `files` (`id`, `name`, `path`, `type`, `size`, `user_id`, `parent_id`, `created_at`, `updated_at`)
VALUES
	(1,'1525404479300_download.jpeg','5-2018','image','original',1,NULL,'2018-05-04 03:27:59','2018-05-04 03:27:59'),
	(2,'1525404479300_download_small.jpeg','5-2018','image','small',1,1,'2018-05-04 03:27:59','2018-05-04 03:27:59'),
	(3,'1525404479300_download_medium.jpeg','5-2018','image','medium',1,1,'2018-05-04 03:27:59','2018-05-04 03:27:59'),
	(4,'1525404479300_download_full.jpeg','5-2018','image','full',1,1,'2018-05-04 03:27:59','2018-05-04 03:27:59'),
	(5,'1525404777989_download.jpeg','5-2018','image','original',1,NULL,'2018-05-04 03:32:58','2018-05-04 03:32:58'),
	(6,'1525404777989_download_small.jpeg','5-2018','image','small',1,5,'2018-05-04 03:32:58','2018-05-04 03:32:58'),
	(7,'1525404777989_download_medium.jpeg','5-2018','image','medium',1,5,'2018-05-04 03:32:58','2018-05-04 03:32:58'),
	(8,'1525404777989_download_full.jpeg','5-2018','image','full',1,5,'2018-05-04 03:32:58','2018-05-04 03:32:58'),
	(9,'1525436403667_P5267.jpeg','5-2018','image','original',1,NULL,'2018-05-04 12:20:03','2018-05-04 12:20:03'),
	(10,'1525436403667_P5267_small.jpeg','5-2018','image','small',1,9,'2018-05-04 12:20:03','2018-05-04 12:20:03'),
	(11,'1525436403667_P5267_medium.jpeg','5-2018','image','medium',1,9,'2018-05-04 12:20:03','2018-05-04 12:20:03'),
	(12,'1525436403667_P5267_full.jpeg','5-2018','image','full',1,9,'2018-05-04 12:20:03','2018-05-04 12:20:03'),
	(13,'1525642603722_beer.png','5-2018','image','original',1,NULL,'2018-05-06 21:36:43','2018-05-06 21:36:43'),
	(14,'1525642603722_beer_small.png','5-2018','image','small',1,13,'2018-05-06 21:36:43','2018-05-06 21:36:43'),
	(15,'1525642603722_beer_medium.png','5-2018','image','medium',1,13,'2018-05-06 21:36:43','2018-05-06 21:36:43'),
	(16,'1525642603722_beer_full.png','5-2018','image','full',1,13,'2018-05-06 21:36:43','2018-05-06 21:36:43'),
	(17,'1525642799738_1525200957592_hocus-pocus-magic-trap_small.jpeg','5-2018','image','original',1,NULL,'2018-05-06 21:39:59','2018-05-06 21:39:59'),
	(18,'1525642799738_1525200957592_hocus-pocus-magic-trap_small_small.jpeg','5-2018','image','small',1,17,'2018-05-06 21:39:59','2018-05-06 21:39:59'),
	(19,'1525642799738_1525200957592_hocus-pocus-magic-trap_small_medium.jpeg','5-2018','image','medium',1,17,'2018-05-06 21:39:59','2018-05-06 21:39:59'),
	(20,'1525642799738_1525200957592_hocus-pocus-magic-trap_small_full.jpeg','5-2018','image','full',1,17,'2018-05-06 21:39:59','2018-05-06 21:39:59'),
	(21,'1525642927476_hocus_pocus.png','5-2018','image','original',1,NULL,'2018-05-06 21:42:07','2018-05-06 21:42:07'),
	(22,'1525642927476_hocus_pocus_small.png','5-2018','image','small',1,21,'2018-05-06 21:42:08','2018-05-06 21:42:08'),
	(23,'1525642927476_hocus_pocus_medium.png','5-2018','image','medium',1,21,'2018-05-06 21:42:08','2018-05-06 21:42:08'),
	(24,'1525642927476_hocus_pocus_full.png','5-2018','image','full',1,21,'2018-05-06 21:42:08','2018-05-06 21:42:08'),
	(25,'1525642972256_capa_bar.png','5-2018','image','original',1,NULL,'2018-05-06 21:42:52','2018-05-06 21:42:52'),
	(26,'1525642972256_capa_bar_small.png','5-2018','image','small',1,25,'2018-05-06 21:42:52','2018-05-06 21:42:52'),
	(27,'1525642972256_capa_bar_medium.png','5-2018','image','medium',1,25,'2018-05-06 21:42:52','2018-05-06 21:42:52'),
	(28,'1525642972256_capa_bar_full.png','5-2018','image','full',1,25,'2018-05-06 21:42:52','2018-05-06 21:42:52'),
	(29,'1525643260656_capa_evento.png','5-2018','image','original',1,NULL,'2018-05-06 21:47:40','2018-05-06 21:47:40'),
	(30,'1525643260656_capa_evento_small.png','5-2018','image','small',1,29,'2018-05-06 21:47:41','2018-05-06 21:47:41'),
	(31,'1525643260656_capa_evento_medium.png','5-2018','image','medium',1,29,'2018-05-06 21:47:41','2018-05-06 21:47:41'),
	(32,'1525643260656_capa_evento_full.png','5-2018','image','full',1,29,'2018-05-06 21:47:41','2018-05-06 21:47:41'),
	(33,'1529269660640_beer.png','6-2018','image','original',1,NULL,'2018-06-17 18:07:40','2018-06-17 18:07:40'),
	(34,'1529269660640_beer_small.png','6-2018','image','small',1,33,'2018-06-17 18:07:40','2018-06-17 18:07:40'),
	(35,'1529269660640_beer_medium.png','6-2018','image','medium',1,33,'2018-06-17 18:07:40','2018-06-17 18:07:40'),
	(36,'1529269660640_beer_full.png','6-2018','image','full',1,33,'2018-06-17 18:07:40','2018-06-17 18:07:40');

/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table friend_requests
# ------------------------------------------------------------

DROP TABLE IF EXISTS `friend_requests`;

CREATE TABLE `friend_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_from_id` int(10) unsigned NOT NULL,
  `user_to_id` int(10) unsigned NOT NULL,
  `status` enum('REQUESTED','CANCELLED','ACCEPTED','DECLINED','BLOCKED') NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `friend_requests_user_from_id_foreign` (`user_from_id`),
  KEY `friend_requests_user_to_id_foreign` (`user_to_id`),
  CONSTRAINT `friend_requests_user_from_id_foreign` FOREIGN KEY (`user_from_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `friend_requests_user_to_id_foreign` FOREIGN KEY (`user_to_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `friend_requests` WRITE;
/*!40000 ALTER TABLE `friend_requests` DISABLE KEYS */;

INSERT INTO `friend_requests` (`id`, `user_from_id`, `user_to_id`, `status`, `created_at`, `updated_at`)
VALUES
	(40,9,8,'ACCEPTED','2018-06-17 19:54:56','2018-06-17 19:58:08'),
	(42,8,5,'ACCEPTED','2018-06-17 19:54:56','2018-06-17 19:58:08');

/*!40000 ALTER TABLE `friend_requests` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table galleries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `galleries`;

CREATE TABLE `galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `order` int(10) unsigned NOT NULL DEFAULT 0,
  `visible` tinyint(1) NOT NULL DEFAULT 1,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `galleries_user_id_index` (`user_id`),
  CONSTRAINT `galleries_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table invitations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `invitations`;

CREATE TABLE `invitations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_from_id` int(10) unsigned NOT NULL,
  `user_to_id` int(10) unsigned NOT NULL,
  `place_id` int(10) unsigned NOT NULL,
  `type` enum('invitation_to_drink') NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invitations_user_from_id_foreign` (`user_from_id`),
  KEY `invitations_user_to_id_foreign` (`user_to_id`),
  KEY `invitations_place_id_foreign` (`place_id`),
  CONSTRAINT `invitations_place_id_foreign` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `invitations_user_from_id_foreign` FOREIGN KEY (`user_from_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `invitations_user_to_id_foreign` FOREIGN KEY (`user_to_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `invitations` WRITE;
/*!40000 ALTER TABLE `invitations` DISABLE KEYS */;

INSERT INTO `invitations` (`id`, `user_from_id`, `user_to_id`, `place_id`, `type`, `created_at`, `updated_at`)
VALUES
	(17,8,9,1,'invitation_to_drink','2018-07-13 21:56:20','2018-07-13 21:56:20'),
	(18,8,9,1,'invitation_to_drink','2018-07-13 21:57:36','2018-07-13 21:57:36');

/*!40000 ALTER TABLE `invitations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table likes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `likes`;

CREATE TABLE `likes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `type` enum('BEER','IMAGE','VIDEO','EVENT','PLACE') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `likes_item_id_index` (`item_id`),
  KEY `likes_user_id_index` (`user_id`),
  CONSTRAINT `likes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table messages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `messages`;

CREATE TABLE `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `from_user_id` int(10) unsigned NOT NULL,
  `to_user_id` int(10) unsigned NOT NULL,
  `text` longtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `messages_from_user_id_index` (`from_user_id`),
  KEY `messages_to_user_id_index` (`to_user_id`),
  CONSTRAINT `messages_from_user_id_foreign` FOREIGN KEY (`from_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `messages_to_user_id_foreign` FOREIGN KEY (`to_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;

INSERT INTO `messages` (`id`, `created_at`, `updated_at`, `from_user_id`, `to_user_id`, `text`)
VALUES
	(1,NULL,NULL,9,8,'lsadl;askdl;askl;dkasl;kd;alsd'),
	(2,NULL,NULL,9,8,'lsadl;askdl;askl;dkasl;kd;alsd'),
	(3,NULL,NULL,8,9,'lsadl;askdl;askl;dkasl;kd;alsd');

/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table notifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('INVITATION_SENDED','INVITATION_ACCEPTED','FRIENDSHIP_ACCEPTED','FRIENDSHIP_REQUESTED','DUEL','MESSAGE') NOT NULL,
  `message` text DEFAULT NULL,
  `elem_id` int(10) unsigned NOT NULL,
  `user_to` int(10) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_elem_id_index` (`elem_id`),
  KEY `notifications_user_to_index` (`user_to`),
  CONSTRAINT `notifications_user_to_foreign` FOREIGN KEY (`user_to`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;

INSERT INTO `notifications` (`id`, `type`, `message`, `elem_id`, `user_to`, `created_at`, `updated_at`)
VALUES
	(1,'INVITATION_SENDED','<p> <b>Falconiere</b> te convidou para beber no <b>Bar do zé</b></p>',4,5,'2018-06-17 18:54:06','2018-06-17 18:54:06'),
	(2,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',12,8,'2018-06-17 19:19:34','2018-06-17 19:19:34'),
	(3,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',13,8,'2018-06-17 19:20:17','2018-06-17 19:20:17'),
	(4,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',14,8,'2018-06-17 19:21:41','2018-06-17 19:21:41'),
	(5,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',15,8,'2018-06-17 19:22:22','2018-06-17 19:22:22'),
	(6,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',16,8,'2018-06-17 19:22:50','2018-06-17 19:22:50'),
	(7,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',17,8,'2018-06-17 19:29:27','2018-06-17 19:29:27'),
	(8,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',18,8,'2018-06-17 19:29:42','2018-06-17 19:29:42'),
	(9,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',19,8,'2018-06-17 19:30:13','2018-06-17 19:30:13'),
	(10,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',20,8,'2018-06-17 19:31:05','2018-06-17 19:31:05'),
	(11,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',21,8,'2018-06-17 19:31:45','2018-06-17 19:31:45'),
	(12,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',22,8,'2018-06-17 19:32:20','2018-06-17 19:32:20'),
	(13,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',23,8,'2018-06-17 19:32:46','2018-06-17 19:32:46'),
	(14,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',24,8,'2018-06-17 19:33:43','2018-06-17 19:33:43'),
	(15,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',25,8,'2018-06-17 19:34:12','2018-06-17 19:34:12'),
	(16,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',26,8,'2018-06-17 19:35:19','2018-06-17 19:35:19'),
	(17,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',27,8,'2018-06-17 19:36:57','2018-06-17 19:36:57'),
	(18,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',28,8,'2018-06-17 19:37:40','2018-06-17 19:37:40'),
	(19,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',29,8,'2018-06-17 19:39:06','2018-06-17 19:39:06'),
	(20,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',30,8,'2018-06-17 19:39:45','2018-06-17 19:39:45'),
	(21,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',31,8,'2018-06-17 19:40:48','2018-06-17 19:40:48'),
	(22,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',32,8,'2018-06-17 19:42:14','2018-06-17 19:42:14'),
	(23,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',33,8,'2018-06-17 19:42:41','2018-06-17 19:42:41'),
	(24,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',34,8,'2018-06-17 19:43:12','2018-06-17 19:43:12'),
	(25,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',35,8,'2018-06-17 19:44:58','2018-06-17 19:44:58'),
	(26,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',36,8,'2018-06-17 19:46:13','2018-06-17 19:46:13'),
	(27,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',37,8,'2018-06-17 19:46:57','2018-06-17 19:46:57'),
	(28,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',38,8,'2018-06-17 19:50:21','2018-06-17 19:50:21'),
	(29,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',39,8,'2018-06-17 19:53:21','2018-06-17 19:53:21'),
	(30,'FRIENDSHIP_ACCEPTED','<p><b>Falconiere</b> aceitou sua pedido de amizade</p>',39,9,'2018-06-17 19:53:56','2018-06-17 19:53:56'),
	(31,'FRIENDSHIP_REQUESTED','<p><b>Falconiere</b> te enviou uma pedido de amizade</p>',40,8,'2018-06-17 19:54:56','2018-06-17 19:54:56'),
	(32,'FRIENDSHIP_ACCEPTED','<p><b>Falconiere</b> aceitou sua pedido de amizade</p>',40,9,'2018-06-17 19:58:08','2018-06-17 19:58:08'),
	(33,'INVITATION_SENDED','<p> <b>Falconiere</b> te convidou para beber no <b>Bar do zé</b></p>',5,9,'2018-06-17 19:58:26','2018-06-17 19:58:26'),
	(34,'INVITATION_SENDED','<p> <b>Falconiere</b> te convidou para beber no <b>Bar do zé</b></p>',6,9,'2018-06-17 19:59:14','2018-06-17 19:59:14'),
	(35,'INVITATION_SENDED','<p> <b>Falconiere</b> te convidou para beber no <b>Bar do zé</b></p>',7,9,'2018-06-17 19:59:35','2018-06-17 19:59:35'),
	(36,'INVITATION_SENDED','<p> <b>Falconiere</b> te convidou para beber no <b>Bar do zé</b></p>',8,9,'2018-06-17 19:59:39','2018-06-17 19:59:39'),
	(37,'INVITATION_SENDED','<p> <b>Falconiere</b> te convidou para beber no <b>Bar do zé</b></p>',9,9,'2018-06-17 20:00:01','2018-06-17 20:00:01'),
	(38,'INVITATION_SENDED','<p> <b>Falconiere</b> te convidou para beber no <b>Bar do zé</b></p>',10,9,'2018-06-17 20:00:23','2018-06-17 20:00:23'),
	(39,'INVITATION_SENDED','<p> <b>Falconiere</b> te convidou para beber no <b>Bar do zé</b></p>',11,9,'2018-06-17 20:00:27','2018-06-17 20:00:27'),
	(40,'INVITATION_SENDED','<p> <b>Falconiere</b> te convidou para beber no <b>Bar do zé</b></p>',12,9,'2018-06-17 20:01:22','2018-06-17 20:01:22'),
	(41,'INVITATION_SENDED','<p> <b>Falconiere</b> te convidou para beber no <b>Bar do zé</b></p>',16,9,'2018-06-17 20:03:26','2018-06-17 20:03:26'),
	(42,'INVITATION_SENDED','<p> <b>Falconiere</b> te convidou para beber no <b>Bar do zé</b></p>',17,9,'2018-07-13 21:56:20','2018-07-13 21:56:20'),
	(43,'INVITATION_SENDED','<p> <b>Falconiere</b> te convidou para beber no <b>Bar do zé</b></p>',18,9,'2018-07-13 21:57:36','2018-07-13 21:57:36');

/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table permission_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permission_roles`;

CREATE TABLE `permission_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `permission_id` int(10) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_roles_role_id_index` (`role_id`),
  KEY `permission_roles_permission_id_index` (`permission_id`),
  CONSTRAINT `permission_roles_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `slug` varchar(191) NOT NULL,
  `description` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table place_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `place_user`;

CREATE TABLE `place_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `place_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `place_user_place_id_index` (`place_id`),
  KEY `place_user_user_id_index` (`user_id`),
  CONSTRAINT `place_user_place_id_foreign` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `place_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table places
# ------------------------------------------------------------

DROP TABLE IF EXISTS `places`;

CREATE TABLE `places` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `address` text NOT NULL,
  `description` text DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `week_range` varchar(191) DEFAULT NULL,
  `hour_range` varchar(191) DEFAULT NULL,
  `type` enum('cerveceria','bar','emporio') DEFAULT NULL,
  `profile_pic` int(10) unsigned DEFAULT NULL,
  `cover_pic` int(10) unsigned DEFAULT NULL,
  `gallery_id` int(10) unsigned DEFAULT NULL,
  `city_id` int(10) unsigned DEFAULT NULL,
  `order` int(10) unsigned DEFAULT 0,
  `visible` tinyint(1) DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `places_city_id_index` (`city_id`),
  CONSTRAINT `places_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `places` WRITE;
/*!40000 ALTER TABLE `places` DISABLE KEYS */;

INSERT INTO `places` (`id`, `name`, `address`, `description`, `phone`, `week_range`, `hour_range`, `type`, `profile_pic`, `cover_pic`, `gallery_id`, `city_id`, `order`, `visible`, `created_at`, `updated_at`)
VALUES
	(1,'Bar do zé','Botafogo, Rio de Janeiro - RJ','Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.','(00) 00000','Segunda a sexta','19hs as 22:00','bar',21,25,NULL,4,0,1,'2018-05-06 21:43:07','2018-05-16 02:46:18'),
	(2,'Emporio do zé','Botafogo, Rio de Janeiro - RJ','Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.','(00) 00000','Segunda a sexta','19hs as 22:00','emporio',21,25,NULL,3,0,1,'2018-05-06 21:43:09','2018-05-16 02:46:27'),
	(3,'Cervejaria do  do zé','Botafogo, Rio de Janeiro - RJ','Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.','(00) 00000','Segunda a sexta','19hs as 22:00','cerveceria',21,25,NULL,3,0,1,'2018-05-06 21:43:10','2018-05-16 02:46:34'),
	(4,'Emporio 2 do zé','Botafogo, Rio de Janeiro - RJ','Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.','(00) 00000','Segunda a sexta','19hs as 22:00','emporio',21,25,NULL,3,0,1,'2018-05-06 21:43:11','2018-05-16 02:46:41'),
	(10,'Bar do zé','Botafogo, Rio de Janeiro - RJ','Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.','(00) 00000','Segunda a sexta','19hs as 22:00','bar',21,25,NULL,3,0,1,'2018-05-06 21:43:21','2018-05-16 02:46:48');

/*!40000 ALTER TABLE `places` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table role_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `role_users`;

CREATE TABLE `role_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_users_role_id_index` (`role_id`),
  KEY `role_users_user_id_index` (`user_id`),
  CONSTRAINT `role_users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `role_users` WRITE;
/*!40000 ALTER TABLE `role_users` DISABLE KEYS */;

INSERT INTO `role_users` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`)
VALUES
	(1,1,1,'2018-05-01 00:35:47','2018-05-01 00:35:47'),
	(2,1,2,'2018-05-01 00:35:47','2018-05-01 00:35:47'),
	(4,2,5,'2018-05-01 00:35:47','2018-05-01 00:35:47'),
	(5,2,8,'2018-05-21 04:33:10','2018-05-21 04:33:10'),
	(6,2,9,'2018-06-17 19:18:01','2018-06-17 19:18:01');

/*!40000 ALTER TABLE `role_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `slug` varchar(191) NOT NULL,
  `description` text DEFAULT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `slug`, `description`, `is_admin`, `created_at`, `updated_at`)
VALUES
	(1,'Administrator','admin',NULL,1,'2018-05-04 01:44:53','2018-05-04 01:44:53'),
	(2,'Default','default',NULL,0,'2018-05-04 01:44:53','2018-05-04 01:44:53');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table saves
# ------------------------------------------------------------

DROP TABLE IF EXISTS `saves`;

CREATE TABLE `saves` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `place_id` int(10) unsigned DEFAULT NULL,
  `event_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `type` enum('place','event') NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `saves_user_id_index` (`user_id`),
  KEY `saves_event_id_foreign` (`event_id`),
  KEY `saves_place_id_foreign` (`place_id`),
  CONSTRAINT `saves_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `saves_place_id_foreign` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `saves_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tokens`;

CREATE TABLE `tokens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `token` varchar(40) NOT NULL,
  `type` varchar(80) NOT NULL,
  `is_revoked` tinyint(1) DEFAULT 0,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tokens_token_unique` (`token`),
  KEY `tokens_user_id_foreign` (`user_id`),
  CONSTRAINT `tokens_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `facebook_id` varchar(255) DEFAULT NULL,
  `google_id` varchar(255) DEFAULT NULL,
  `beer_id` int(10) unsigned DEFAULT NULL,
  `beer_type_id` int(10) unsigned DEFAULT NULL,
  `profile_pic_url` varchar(255) DEFAULT NULL,
  `cover_pic_url` varchar(255) DEFAULT NULL,
  `last_login_by` enum('facebook','google','api') NOT NULL DEFAULT 'api',
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `profile_pic_id` int(10) unsigned DEFAULT NULL,
  `cover_pic_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_facebook_id_unique` (`facebook_id`),
  UNIQUE KEY `users_google_id_unique` (`google_id`),
  UNIQUE KEY `users_username_unique` (`username`),
  KEY `users_beer_type_id_foreign` (`beer_type_id`),
  KEY `users_beer_id_foreign` (`beer_id`),
  KEY `users_profile_pic_id_foreign` (`profile_pic_id`),
  KEY `users_cover_pic_id_foreign` (`cover_pic_id`),
  CONSTRAINT `users_beer_id_foreign` FOREIGN KEY (`beer_id`) REFERENCES `beers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_beer_type_id_foreign` FOREIGN KEY (`beer_type_id`) REFERENCES `beer_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_cover_pic_id_foreign` FOREIGN KEY (`cover_pic_id`) REFERENCES `files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_profile_pic_id_foreign` FOREIGN KEY (`profile_pic_id`) REFERENCES `files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `lastname`, `facebook_id`, `google_id`, `beer_id`, `beer_type_id`, `profile_pic_url`, `cover_pic_url`, `last_login_by`, `username`, `email`, `password`, `created_at`, `updated_at`, `profile_pic_id`, `cover_pic_id`)
VALUES
	(1,'Administrator','viking',NULL,NULL,NULL,NULL,NULL,NULL,'api','admin','admin@vikings.com','$2a$10$uskaXcayIKp1Q0y/E44Sfe.tYjKQ3nWDa9dNcqDpXkCYtxO9Uik/G','2018-05-04 01:44:53','2018-05-04 01:44:53',NULL,NULL),
	(2,'System','viking',NULL,NULL,NULL,NULL,NULL,NULL,'api','system','system@vikings.com','$2a$10$oE30WGoqu4vooeFFALT9A.h/UBMflKhwigo2vfRO.3Z9ylqCwjsMi','2018-05-04 01:44:53','2018-05-04 01:44:53',NULL,NULL),
	(5,'Carlos','Alvarez','1505153676260268',NULL,NULL,NULL,'https://graph.facebook.com/1505153676260268/picture?type=large','https://lookaside.facebook.com/platform/coverpic/?asid=1505153676260268&ext=1525972137&hash=AeR7EsUWlK1xgAsJ','api',NULL,'carlosxviii@gmail.com','$2a$10$oE30WGoqu4vooeFFALT9A.h/UBMflKhwigo2vfRO.3Z9ylqCwjsMi','2018-05-07 17:08:59','2018-05-07 17:08:59',NULL,NULL),
	(8,'Falconiere','Barbosa','2094582844103953',NULL,NULL,NULL,'https://graph.facebook.com/2094582844103953/picture?type=large','https://graph.facebook.com/2094582844103953/cover?type=large','api',NULL,'falconieer@gmail.com','$2a$10$oE30WGoqu4vooeFFALT9A.h/UBMflKhwigo2vfRO.3Z9ylqCwjsMi','2018-05-21 04:33:10','2018-05-21 04:33:10',NULL,NULL),
	(9,'Falconiere','Rodrigues Barbosa','2009576182691827',NULL,NULL,NULL,'https://graph.facebook.com/2009576182691827/picture?type=large&width=500&height=500','https://graph.facebook.com/2009576182691827/cover?type=large','api',NULL,'falconieer@outlook.com','$2a$10$oE30WGoqu4vooeFFALT9A.h/UBMflKhwigo2vfRO.3Z9ylqCwjsMi','2018-06-17 19:18:01','2018-06-17 19:18:01',NULL,NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
