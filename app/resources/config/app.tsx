require('../assets/sass/app.scss');


import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as PropTypes from 'prop-types';

import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';

import { createBrowserHistory } from 'history'
import {routerMiddleware}  from 'connected-react-router'
import  {connectRouter} from 'connected-react-router'
import { composeWithDevTools } from 'redux-devtools-extension';



import logger from 'redux-logger'
import reducers from './reducers'



/**
 * User token
 */
const globalAny:any = global;

globalAny.baseApi = "http://api.vikings.local/api/v1";



const history = createBrowserHistory()
/**
 * Create the store
 */
const store : any =  createStore(
  connectRouter(history)(reducers), // new root reducer with router state
  {}, //initial state
  composeWithDevTools(
    applyMiddleware(
      logger,
      thunk,
      routerMiddleware(history), // for dispatching history actions
    ),
  ),
)
/**
 * Imports routes
 */
import Routes from './routes';

//Define app
const App : any = (props:any)=>(
  <Provider store={props.store}>
    <Routes history={props.history}/>
  </Provider>
)

App.propTypes = {
  store: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
}
ReactDOM.render(<App store={store} history={history} />, document.getElementById("root"))