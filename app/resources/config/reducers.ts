import { combineReducers } from 'redux'


/**
 * Reducers
 */
import { routerReducer as routing } from 'react-router-redux';
import EntryTextReduxReducer from "../components/entry_text/js/EntryTextReduxReducer";
import FormViewReduxReducer from "../components/form_view/js/FormViewReduxReducer";
import ChatUserRowReduxReducer from "../components/chat_user_row/js/ChatUserRowReduxReducer"
import ChatReduxReducer from "../views/chat/js/ChatReduxReducer"



/**
 * Export the reducers
*/
export default combineReducers({
  EntryTextReduxReducer,
  FormViewReduxReducer,
  ChatUserRowReduxReducer,
  ChatReduxReducer,
  routing
});