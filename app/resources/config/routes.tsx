import * as React from 'react';
import {Switch, Route , Redirect} from "react-router-dom";
import { ConnectedRouter } from 'connected-react-router'

/**
 * Import views (containers)
 */
import Login from '../views/login/Login';
import Home from '../views/home/home';
import UserForm from '../views/user_form/UserForm';
import Chat from '../views/chat/Chat';

/**
 * Import components 
*/
import SideBar from '../components/side_bar/Sidebar';

/**
 * Check is auteticate
 * @returns {Boolean}
 */
const isAuteticated = ()=>{
  let token = localStorage.getItem("api_token");
  if(!token || token==null || token=="null"){
    return false
  }
  return true
}

/**
 * Return the private routes
 * @param props 
 * @returns {Object}
 */
const getPrivateRoutes = (props:any)=>{
  
  if(isAuteticated()){
    //props.history.push("/home",this.state);
    return(
      <div className="container-app">
          <div className="container-side-bar">
            <SideBar />
          </div>
          <div className="container-pager">
            <Switch>
              <Route path="/" component={Home} exact={true}  />
              <Route path="/home" component={Home} exact={false}  />
              <Route path="/chat" component={Chat} exact={true}   />
              <Route path="/chat/:userId" component={Chat} exact={true} />
              <Route path="/form/user" component={UserForm} exact={false} />
            </Switch>
          </div>
      </div>
    )
  }
  props.history.push("/login",this.state);
  return <Login />
}

/**
 * Definet the container of routes
 * @param props 
 * @returns {Object}
 */
const Routes = (props:any) =>  {
  return (
    <ConnectedRouter history={props.history}>
      <Switch>
        <Route path="/logout" component={Login} />
        <Route path="/login" component={Login} />
        <Route path="/" component={getPrivateRoutes} ></Route>
        <Redirect from="/" to="/home"   />
      </Switch>
    </ConnectedRouter>
  )
}
export default Routes;
