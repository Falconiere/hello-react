import UserControllerApi from "./UserControllerApi";
import * as openSocket from 'socket.io-client';

//declare var baseApi : string
//const api ='/api/v1'
const host = `ws://192.168.0.5:3333`

interface Promise{
  then?:Function,
  catch?:Function,
}

export default class ChatControllerApi{

  private webSocket : any;
  
  constructor(){
    
    let userData = this.getUserData()
    const socket = openSocket(`${host}`, {
      query: userData
    })
    this.webSocket = socket.connect()
  }
  private getUserData() : Object{
    let userData = JSON.parse(localStorage.getItem("user_data"))
    return userData
  }
  public getFriends(userId:number) : Promise{
    return UserControllerApi.getFriends(userId)
  }

  public sendMessage(message:any) : void{
    this.webSocket.emit("onMessage",message)
  } 

  public receiveMessage(callBack:Function) : void{
     this.webSocket.on("onReceiveMessage",(message:any)=>{ callBack(message)})
  }

  public onFriendTyping(callBack:Function) : void{
    this.webSocket.on("onFriendTyping",(data:any)=>{ callBack(data)})
  }
  
  public isTypingMessage(isTyping:boolean,friend:any) : void{
    let userData = this.getUserData()
    this.webSocket.emit("onTypingMessage",{from:userData,to:friend,isTyping:isTyping})
  }
}