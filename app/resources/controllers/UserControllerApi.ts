import RequestHelper from "../helpers/RequestHelper"



declare var baseApi : string
interface Promise{
  then?:Function,
  catch?:Function,
}
export default  class UserControllerApi{

  /**
   * The getFriends is responsable by to get the friends forom user
   * 
   * @param {Number} userId 
   * @returns {Promise}
   */
  static getFriends(userId:Number,page:Number = 1) : Promise{
    return RequestHelper.Get(`${baseApi}/friends/${userId}?page=${page}`)
  }
  
  static loginUser(body:object){
    return RequestHelper.Post(`${baseApi}/login/`,body)
  }
}