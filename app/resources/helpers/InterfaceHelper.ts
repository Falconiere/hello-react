export interface IPromise{
  then?:Function,
  catch?:Function
}