import axios from 'axios';
/*
|--------------------------------------------------------------------------
| API Responses header code
|--------------------------------------------------------------------------
|
| 200 All is ok
| 401 Unauthorized, there is no valid api_token on the header
| 403 Don't have permission to perform the action
| 422 Unprocessable entity, the validation process of the sended that has failed
|
 */

 

declare var Promise : any
interface Promise{
  then?:Function,
  catch?:Function,
}

export default class RequetHelper{
  

  /**
   * The function 'Get' is responsable by
   * to make request get using a request get
   * @param url : String
   * @param params : Object
   * @returns  Promise
   */
  static Get(url:string,params={},responseType='json') : Promise{
    return this.Requester(url,'GET',params,responseType);
  }

  /**
   * The function 'Post' is responsable by
   * to make post request using a request post
   * @param url : String
   * @param params : Object
   * @returns  Promise
   */
  static Post(url:string,params={},responseType='json') :  Promise{
    return this.Requester(url,'POST',params,responseType);
  }

  /**
   * The function 'Put' is responsable by
   * to make Put request using a request post
   * @param url : String
   * @param params : Object
   * @returns  Promise
   */
  static Put(url:string,params={},responseType='json') : Promise{
    return this.Requester(url,'PUT',params,responseType);
  }

  /**
     *  The function 'Delete' is responsable by
     * to make delete request using a request post
     * @param url : String
     * @param params : Object
     * @returns  Promise
    */
  static Delete(url:string,params={},responseType='json') : Promise {
    return this.Requester(url,'DELETE',params,responseType);
  }


  
  static Requester(url:string,method:any,params={},responseType='json') : Promise {
      let self = this;
      let api_token : string = localStorage.getItem("api_token");
      let promise=  new Promise((resolve:Function,reject:Function)=>{
        axios({
            method: method,
            url:url,
            headers: {
              'Authorization': `Bearer ${api_token}`,
            },
            data: params,
            responseType:responseType
          }).then((success:any)=>{
            resolve(self.handleResponse(success));
          },(error:any)=>{
            reject(self.handleResponse(error.response));
          });
      });
    
    return promise
  }
  /**
   *  The handleResponse  is responsable by
   *  to parse the response from resquest and translate
   *  to front-end
   *  @param JsonResponse (ex: error.response )
   *  @return Object
   */
    static handleResponse(response:any) : Object{
        if(!response)
        return;
        
        var code = response && response.hasOwnProperty("status") ? response.status : 401;
        var newResoponse = {};

        switch(code){
        case 200 :
            var data = response.data ;
            newResoponse = { status:"ok", statusCode:code, message:response.data.msg, data:data};
            break;

        case 401 :
            var data = response.data ;
            newResoponse = { status:"nok",statusCode:code,  message:"No posee autorización", data:data};
            break;

        case 403 :
            var data = response.data ;
            newResoponse = { status:"nok", statusCode:code,  message:"No cuenta con los permisos necesarios" , data:data};
            break;

        case 422 :

            //build the body of messages
            var message = "<h3>Existen parametros inválidos:</h3> <br> ";
            if(Object.keys(response.data).length>0){
            var data = response.data;
              for(var key in data ){
                  message += "=> <strong class='info-error'>"+ data[key][0] +"</strong><br> ";
              }
            }
            newResoponse = { status:"nok", statusCode:code, message:message, data:response.data };
            break;
        default:
            newResoponse = { status:"nok", statusCode:code,  message:"Error desconocido", data:response.data };
            break;
        }
        return newResoponse;
    }
}

