export  function entryTextChangeText(keyModel:string,model:any){
    return {
      type: "CHANGE_ENTRYTEXT_MODEL",
      keyModel: keyModel,
      model: model,
    }
}