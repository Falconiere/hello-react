import * as React from 'react';
import * as PropTypes from 'prop-types';
import {entryTextChangeText} from './EntryTextReduxActions';

export default class EntryTextUi extends React.Component {
  
  state : any
  props: any
  /**
   * The constructor from side bar with super props
   * and variable definitions
   * @param  {OBject} props 
   * @return {null}      
   */
  constructor(props:any) {
    super(props);
    this.state = props;
    this.onChange = this.onChange.bind(this);
    
 
  }
  /**
   * The onChange is respnsable  by to listen changes from input
   * @return {null}
   */
  onChange(e:any){
    let model = this.state.model;
    model.val = e.target.value;
    this.setState({model:model});
    this.props.dispatch(entryTextChangeText(this.state.keyModel,this.state.model));
    this.props.change(this.state.keyModel,this.state.model);
  }

  /**
   * The propTypes defination
   * @type {Object}
   */
  static propTypes = {
    model: PropTypes.object.isRequired,
    keyModel: PropTypes.any.isRequired,
    change: PropTypes.func.isRequired
  };
}



 


