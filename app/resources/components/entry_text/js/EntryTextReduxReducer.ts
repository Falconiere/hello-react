declare var Object : any
export default function EntryTextReduxReducer(state:any = [], action:any){

  switch (action.type) {
    case "CHANGE_ENTRYTEXT_MODEL":
      return Object.assign({}, state, {
        model: action.model,
        keyModel:action.keyModel
      })
     break;
    default:
      return state
  }

}