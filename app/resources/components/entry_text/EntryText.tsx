import * as React from 'react';
import {connect} from 'react-redux'
import EntryTextUi from './js/EntryTextUi';

class EntryText extends EntryTextUi{
	render(){
		return (
			<div className="entry-text form-group">
        <fieldset>
  				<label className="row">{this.state.model.label}</label>
					<input	type="text"  value={this.state.model.val} onChange={this.onChange}/>
        </fieldset>
			</div>
		)
	}
}
export default connect()(EntryText);