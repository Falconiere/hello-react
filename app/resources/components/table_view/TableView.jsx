import React from 'react';


/**
 * Controller Ui
 */
import TableViewUi from  "../js/TableViewUi.js";

export default class TableView extends TableViewUi {


  getTheSchemeViewModel(){

  }

  getColumnHeadTable(){
    
  }

  buildTheHeadTable(){

  }

  buildTheBodyTable(){

  }

  render(){
    return (
      <div className="table-view">
        <div className="thead"></div> 
        <div className="tbody"></div> 
      </div>
    );
  }

}
