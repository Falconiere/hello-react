/**
 * Dependencias
 */
import * as React from "react";
import {connect} from 'react-redux'

/**
 * Controllers
 */
import ChatUserRowUi from "./js/ChatUserRowUi"

/**
 * Interfaces
 */
import {IStateChatUserRow} from "./js/ChatUserRowInterface"

class ChatUserRow extends ChatUserRowUi{
  
  public state:IStateChatUserRow

  public render(){
    return (
      <div className="chat-user-row" onClick={this.onTapUserRow}>
        <div className="row">
          <div className="col-md-2">
            <div className="profile-pic">
              <img src={this.state.profilePic} alt=""/>
            </div>
          </div>
          <div className="col-md-10">
            <h3>{this.state.userName}</h3>
            {this.state.isTyping ? <span>Typing...</span> : ""}
          </div>
        </div>
      </div>
    )
  }
}
export default connect()(ChatUserRow);