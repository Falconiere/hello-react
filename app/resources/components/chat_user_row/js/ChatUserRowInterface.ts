export interface IStateChatUserRow{
  profilePic?:string
  userName?:string,
  userId?:number,
  dispatch?:Function,
  onTapRow?:Function,
  isTyping?:Boolean
}