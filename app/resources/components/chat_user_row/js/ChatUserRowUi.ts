import * as React from "react"
import * as PropTypes from "prop-types"

/**
 * Interface
 */
import {IStateChatUserRow} from "./ChatUserRowInterface"

/** 
 * Actions
 */
import {chatUserRowWasTapped} from "./ChatUserRowReduxActions"


export default class ChatUserRowUi extends React.Component<IStateChatUserRow,IStateChatUserRow>{
  
  public state : IStateChatUserRow
  public props: IStateChatUserRow

  constructor(props:any){
    super(props)
    this.state = props
    this.onTapUserRow = this.onTapUserRow.bind(this);
  }

  public onTapUserRow() : void{
    this.props.dispatch(chatUserRowWasTapped(this.state));
    this.props.onTapRow(this.state)//send the state to parent component
  }

  public componentWillReceiveProps(nextProps:IStateChatUserRow) : void{
    this.setState({isTyping:nextProps.isTyping})
  }
  
  /**
   * The properties
   */
  static propTypes : any = {
    profilePic: PropTypes.string,
    userName: PropTypes.string.isRequired,
    userId: PropTypes.number.isRequired,
    onTapRow:PropTypes.func.isRequired,
    isTyping:PropTypes.bool
  }
}

