import * as React from 'react';
import {Link} from "react-router-dom";
import {connect} from 'react-redux'

/**
 * Controller Ui
 */
import SideBarUi from './js/SideBarUi'


class SideBar extends SideBarUi {

	/**
	 * The renderTheLisOfLinks is responsable by to create 
	 * the list of links
	 * @method renderTheLisOfLinks
	 * @return {HTML Object} 
	 */
	private renderTheLisOfLinks() : Object {
		let self = this;
		let arraylinks = self.getTheViewModel();
		let links : Array<Object> = [];

		arraylinks.map((item,index) => {
			links.push(<li key={index}><Link to={item.path} className="link">{item.title}</Link></li>)
		});
		links.push(<li key={arraylinks.length}><Link to="/logout" className="link">Logout</Link></li>)
		return links
  }

  /**
   * The render is natiave function from React
   * @method render
   * @return {HTML Object} 
   */
  public render () : object {
    return(
   		<div className="side-bar"> 
   			<ul>{this.renderTheLisOfLinks()}</ul>
 			</div>
		)
  }
}

export default connect()(SideBar);