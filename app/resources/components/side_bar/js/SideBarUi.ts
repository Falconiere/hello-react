import * as React from 'react';
import SideBarViewModel from './SideBarViewModel';
import * as PropTypes from 'prop-types';

export default class SideBarUi extends React.Component {

	/**
   * The constructor from side bar with super props
   * and variable definitions
   * @param  {Object} props 
   * @return {void}      
   */
  constructor(props:any) {
    super(props);
  }
  /**
   * The componentDidMount is a native function from react-component 
   * @return {void} 
   */
  public componentDidMount() : void {
   
  }
  /**
   * The componentWillUnmount is a native function from react-component 
   * @return {null} 
   */
  public componentWillUnmount() : void {
    
  }
  /**
   * The getTheViewModel is responsale by to het the view model
   * @method getTheViewModel
   * @return {Array} return a array of object
  */
  public getTheViewModel(){
		return SideBarViewModel();
  }
  /**
   * The the props
  */
  static propTypes = {
    descriptionPage: PropTypes.string
  };
}


