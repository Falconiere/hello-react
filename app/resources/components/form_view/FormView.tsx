import * as React from 'react';
import {connect} from 'react-redux'

/**
 * Controller Ui
 */
import FormViewUi from './js/FormViewUi'


/**
 * COmponents
 * 
 */
import EntryText  from '../entry_text/EntryText';

class FormView extends FormViewUi {


	/**
	 * The getEntryType is responsable by to get entry type 
	 * from form view
	 * @param  {string} type  
	 * @param  {object} model  
	 * @param  {string} keyModel   
	 * @return {COMPONENT React}       
	 */
	getEntryType(type:string,keyModel:string,model:any){

		switch(type){
			case null:
				return <span>{model[keyModel].val}</span> 
			break;
			case "text":
				return <EntryText keyModel={keyModel} model={model[keyModel]} change={this.onChangeInput}/>
			break
		}
	}




  /**
   * The buildFormByScheme is responsable by to build the form view
   * by schemeVIew 
   * @return {REACT DOM}
   */
	buildFormByScheme(){

		let schemeView = this.getSchemeViewModel();
		let viewModel = this.getViewModel();
		let rows = schemeView.rows;

		let rowsTemplate :any = [];
		let columnsTemplate : any = [];

		
		for(let r in rows){
				let row = rows[r];
				let columns = rows[r].columns;

				if(!columnsTemplate.hasOwnProperty(r)){
					columnsTemplate[r] = [];	
				}

				/**
				 * Build the rows
				 */
				rowsTemplate.push(<div key={r} className={row.className}>{columnsTemplate[r]}</div>)

				/**
				 * Build the columns
				 */
				columns.map((column:any,index:number) =>{
					columnsTemplate[r].push(<div key={index} className={column.className}>{this.getEntryType(column.entryType,column.key,viewModel)}</div>)
				});
		}

		return rowsTemplate;
	}

	/**
	 * The render is a native method from react 
	 * reference: https://reactjs.org/docs/rendering-elements.html
	 * @return {REACT DOM} 
	 */
	render(){
		return (
			<div className="form-view">
				{this.buildFormByScheme()}
				<button type="button" onClick={this.sendForm.bind(this)}>Send Form</button>
			</div>	
		)
	}
}

export default connect()(FormView)
