import * as React from 'react';
import * as PropTypes from 'prop-types';
import {formViewSendForm} from './FormViewReduxActions';

export default class FormViewUi extends React.Component{


  state : any
  props : any

	constructor(props:any) {
    super(props);
    this.state = props;
    this.onChangeInput = this.onChangeInput.bind(this);
  }


	/**
	 * The getSchemeViewModel is responsable by to}
	 * get the scheme to build form
	 * @return {[type]} [description]
	 */
  public getSchemeViewModel(){
 		return this.state.schemeView;
 	}

 	/**
 	 * The getViewModel is responsable by to get the model from
 	 * view
 	 * @return {Object} 
 	 */
  public getViewModel(){
		return this.state.schemeView.model;  	
  }

  public onChangeInput(key:string,obj:any){
  	this.state.onEventFromFormView(key,obj);
  }

  /**
   * The sendForm is reponsable by to send the form
   * @param  {DOM Object} e [description]
   * @return {null}   [description]
   */
  public sendForm(e:Object){
    console.log(e)
    this.state.dispatch(formViewSendForm(this.getViewModel()));
  }

  static propTypes = {
    schemeView: PropTypes.object,
    onEventFromFormView:PropTypes.func
  };
}




