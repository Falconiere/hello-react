/**
 * Dependences
 */
import * as React from 'react';
import {connect} from 'react-redux'

import ChatUserMessageRowUi from "./js/ChatUserMessageRowUi"

class ChatUserMessageRow extends ChatUserMessageRowUi{

  public render(){
    return(
      <div className="chat-user-message-row">
        <div className="row">
          <div className="profile-pic">{this.state}</div>
          <div className="messge"></div>        
        </div>
      </div>
    )
  }
}
export default connect()(ChatUserMessageRow);
