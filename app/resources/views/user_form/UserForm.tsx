/**
 * Dependences
 */
import * as React from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux'


/**
 * Controllers
 */
import UserFormUi from './js/UserFormUi';


/**
 * Components
 */
import FormView from '../../components/form_view/formview'; 

class UserForm extends UserFormUi {

  /**
   * The render is native method from React.Component,
   * reference: https://reactjs.org/docs/react-component.html  
   * @returns {ReactDOM}
   */
  public render(){
    return (
     <div className="user-form">
       <FormView  schemeView={this.userSchemeViewModel} onEventFromFormView={this.onEventFromFormView} />
      </div>
    )
  }
}
export default withRouter(connect()(UserForm) as any)

