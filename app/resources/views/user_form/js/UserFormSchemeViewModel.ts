import UserViewModel from './UserViewModel';

import { isNull } from 'util';

export default function UserFormSchemeViewMode (){
	return {
		"className":isNull,
    "title":isNull,
      "rows":[
        {
          "className":"row",
          "columns":[
            {"key":"name","entryType":"text","type":isNull,"className":"col-lg-2 col-md-2 col-sm-2 ","visible":true},
            {"key":"email","entryType":"text","type":isNull,"className":"col-lg-2 col-md-2 col-sm-2 ","visible":true},
            {"key":"phone","entryType":"text","type":isNull,"className":"col-lg-2 col-md-2 col-sm-2 ","visible":true},
            {"key":"apellido","entryType":"text","type":isNull,"className":"col-lg-2 col-md-2 col-sm-2 ","visible":true}
          ]
        }
     ],
     model: UserViewModel()
	}
}

