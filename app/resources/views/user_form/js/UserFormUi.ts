import * as React from 'react';
import UserFormSchemeViewModel from './UserFormSchemeViewModel';


export default class UserFormUi extends React.Component {
  
  userSchemeViewModel: any
  
	constructor(props:any) {
    super(props);

    /**
     * Define the scheme view form
     */
    this.userSchemeViewModel =  UserFormSchemeViewModel();
  }

 	
 	/**
 	 * The onEventFromFormView is responsable by to listen
 	 * events from FormView
 	 * @param  {string} key 
 	 * @param  {object} obj 
 	 * @return {null}     
 	 */
  onEventFromFormView(key:string,obj:object){
    console.log(key)
    console.log(obj)
  }
}
 


