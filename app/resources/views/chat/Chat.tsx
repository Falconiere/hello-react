/**
 * Dependeces from react
 */

import * as React from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux'


/**
 * Controller from view
 */
import ChatUi from './js/ChatUi'


/**
 * Declare the interfaces 
 */
import {IChatState} from "./js/ChatInterface"


/**
 * Components
 */
import ChatUserRow from "../../components/chat_user_row/ChatUserRow"


class Chat extends ChatUi{

   public state:IChatState
   
   private renderTheMessages(){
      let listOfMessages :  Array<Object> = []
      let userTo:any = (this.getCurrentUserById(this.state.currentChatUserId))
      let userFrom: any = this.getUserFrom()
      let messages = userTo!=null && userTo.hasOwnProperty("messages") ? userTo.messages : []
    
      if(messages.length > 0){
        messages.forEach((element:any,index:number) => {

          let pic = userFrom.profile_pic_url 
          if(element.from==userTo.id){  
            pic = userTo.profile_pic_url 
          }
          let profilePic = <div className="profile-pic"><img src={pic}/> </div>
          listOfMessages.push(<li key={index} className="item-message">{profilePic}{element.text}</li>)
        });
      }
      return <ul className="messages">{listOfMessages}</ul>
   }
  /**
   * The renderListOfFriends is reponsable by to 
   * render the list of friends
   * @returns {ReactDOM}
   */
  private renderListOfFriends() : Object{
    let listOfFiends: Array<Object> = []
    this.state.friends.forEach((element:any,index:number) => {
      listOfFiends.push(<ChatUserRow 
        key={index} 
        userId={element.id}
        isTyping={element.isTyping}
        profilePic={element.profile_pic_url}  
        userName={element.name} 
        onTapRow={this.onTapUserRow}
        />
      )
    });
    return <div className="users-friends">{listOfFiends}</div>
  }

  private renderTheHeaderFriendInfo() : Object{
    try {
      let userTo:any = (this.getCurrentUserById(this.state.currentChatUserId))
      let pic = <img src={userTo.profile_pic_url} alt=""/>
      let name = <h3>{userTo.name}</h3>  
      return <div className="chat-header-info">{pic}{name}</div>  
    } catch (error) {
      return null
    }
    
  }

  private renderTheEmptyScreenChat() : Object{

    if(!this.state.wasSeletedFriend){
      return(
        <div className="no-chat">
          <h1>Select a friend</h1>
        </div>
      )
    }
    return null
  }
  /**
   * The render is native method from React.Component,
   * reference: https://reactjs.org/docs/react-component.html  
   * @returns {ReactDOM}
   */
  
  public render() : Object{
    
    return (
      <div className="chat chat-container">
      	<div className="col-md-2 col-of-friends">
      		<div className="row">Friends</div>
          <div className="row">
            {this.renderListOfFriends()}
          </div>
      	</div>
      	<div className="col-md-10 col-content-chat">
          {/* This block is for when is not a users seleted */}
          {this.renderTheEmptyScreenChat()}
          <div className="row row-header-chat">
            {this.renderTheHeaderFriendInfo()}
          </div>
          <div className="row row-content-chat">
            {this.renderTheMessages()}
          </div>
          <div className="row row-input-chat">
            <div className="col-md-10">
              <input type="text" value={this.state.currentMessage} className="message-input" onChange={this.onTypingMessage} onKeyPress={this.onPressEnter}/>
            </div>
            <div className="col-md-2">
              <button type="button" className="btn btn-success" onClick={this.onSendMessage}>Enviar</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default withRouter(connect((state)=>({ ...state }))(Chat) as any)
