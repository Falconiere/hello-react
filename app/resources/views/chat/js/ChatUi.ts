/**
 * dependeces
 */
import * as React from 'react';


/**
 * Controllers
 */
import ChatControllerApi from "../../../controllers/ChatControllerApi";

/**
 * The interfaces
 */
import {IChatState} from "./ChatInterface"


/**
 * The actions
 */
import {chatSelectedFriend,chatSendMessageToFriend,chatReceiveMessageFromFriend,chatGetFriends,chatOnFriendTypingMessage} from "./ChatReduxActions";


export default class ChatUi extends React.Component<any,IChatState>{
  
  public state : IChatState
  private ChatControllerApi : ChatControllerApi
  private timOutTyping : any
  private isTyping: boolean

  

  constructor(props:any) {
    super(props)

    this.timOutTyping  = null
    this.isTyping = false
    this.state = {friends:[],currentMessage:"",messages:[],wasSeletedFriend:false,isMonted:false}
 
    /**
     * Set events
    */
    this.onSendMessage = this.onSendMessage.bind(this)
    this.onTypingMessage = this.onTypingMessage.bind(this);
    this.onTapUserRow = this.onTapUserRow.bind(this)
    this.onPressEnter = this.onPressEnter.bind(this)
    
  }
  /**
   * The getFriends is responsable by to get the friends
   * from user
   * @return  {void}
  */
  public getFriends() : void {
    let self = this
    let userData = JSON.parse(localStorage.getItem("user_data"));
    let friends = (this.state.friends);
    this.props.dispatch(chatGetFriends(friends,this.ChatControllerApi.getFriends(userData.id),
      (friends:any)=>{
        self.setState({friends:friends})
        self.checkUserByRoute()
    }));
  }

  protected getUserFrom() : Object{
    return JSON.parse(localStorage.getItem("user_data"));
  }
  
  /**
   * The onSendMessage is responsable 
   * send the message
   * @return  {void}
   */
  public onSendMessage() : void{
    if(this.state.currentMessage!=null && this.state.currentMessage.length > 0){
      let userData = JSON.parse(localStorage.getItem("user_data"))
      let message = {text:this.state.currentMessage,profile_pic_url:userData.profile_pic_url, from:userData.id, to:this.state.currentChatUserId}
      let friends = (this.state.friends)
      this.props.dispatch(chatSendMessageToFriend(friends,message))
      this.ChatControllerApi.sendMessage(message)
      this.setState({currentMessage:"",friends:friends})
    }
  }
  
  /**
   * The onReceiveMessage is responsable by to liste the 
   * message receive 
   * 
   * @param   {any} message 
   * @return  {void}
   */
  public onReceiveMessage(message:any) : void{
    let friends = (this.state.friends)
    let self = this
    this.props.dispatch(chatReceiveMessageFromFriend(friends,message),()=>{
      self.setState({friends:friends})
    })
    
  }

  public onPressEnter(e:any) : void{
    if (e.key === 'Enter') {
      this.onSendMessage()
    }
  }
  /**
   * The onTypingMessage is responsable by to listen the typing message
   * 
   * @param   {any} e  DOM Object
   * @return  {Void}
   */
  public onTypingMessage(e:any) : void{
    let self= this
    this.setState({currentMessage:e.target.value });

    if(this.timOutTyping!=null){
      clearTimeout(this.timOutTyping)
    }
    if(!this.isTyping){
      this.isTyping = true
      this.ChatControllerApi.isTypingMessage(this.isTyping,this.getCurrentUserById(this.state.currentChatUserId))
    }
    this.timOutTyping = setTimeout(function(){
      self.isTyping = false
      self.ChatControllerApi.isTypingMessage(self.isTyping,self.getCurrentUserById(self.state.currentChatUserId))
    },1000)
  }
  /**
   * The onTapUserRow is responsable by to listen when the row user was tapped
   * @param user {Object}
   * @returns {void}
   */
  public onTapUserRow(user:any) : void{
    
    /**
     * Change the state
     */
    this.setState({wasSeletedFriend:true,currentChatUserId:user.userId,currentChatUserName:user.userName})

    /**
     * Change the route
     */
    this.props.history.push(`/chat/${user.userId}`, this.state )
    /**
     * Dispatch event
     */
    this.props.dispatch(chatSelectedFriend({userId:user.userId,userName:user.userName}))
  }

  /**
   * The startChat is responsable by to start the chat 
   * @return  {void}
   */
  private startChat() : void{
    let self = this
    this.ChatControllerApi = new ChatControllerApi()
    this.ChatControllerApi.receiveMessage((messaje:any)=>self.onReceiveMessage(messaje))
    this.ChatControllerApi.onFriendTyping((data:any)=>self.onFriendTyping(data))
    this.getFriends()
   
  }

  private onFriendTyping(data:any) : void{
    let friends = this.state.friends
    this.props.dispatch(chatOnFriendTypingMessage(friends,data.from,data.isTyping))
    this.setState({friends:friends})
  }
  /**
   * The getCurrentUserById is reponsable by to get the user/friend by id
   * @param userId 
   * @returns {Object}
   */
  protected getCurrentUserById(userId:Number) : Object{
    try {
      let friends = this.props.location.state.friends
      return friends.filter((friend:any)=> friend.id==userId)[0]  
    } catch (error) {
      return null
    }
  }
  /**
   * 
   */
  private checkUserByRoute() : void {
    try {
      let params : any =  this.props.match.params
      if(params.hasOwnProperty("userId") && (params.userId as number) > 0){
        let friend : any =  this.getCurrentUserById(params.userId);
        this.props.dispatch(chatSelectedFriend({userId:params.userId,userName:friend.name}))
        this.props.history.push(`/chat/${params.userId}`, this.state )
        //this.props.history.pushState(this.state,null,`/chat/${params.userId}`)
        this.setState({currentChatUserId:params.userId,wasSeletedFriend:true})
      }  
    } catch (error) {
      console.log(error)
    }
  }
  /**
   * The componentDidMount is a native function from react-component 
   * @return {void} 
  */
  public componentDidMount(): void {
    /**
     * Start the chat
     */
    this.startChat()
  }
  /**
   * The componentWillMount is a native function from react-component 
   * @return {void} 
  */
  public componentWillMount() : void {
     
  }
}
