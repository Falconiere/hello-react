declare var Object : any
export default function ChatUserRowReduxreducer(state:any = [], action:any){
  switch (action.type) {
    case "CHAT_SELECTED_FRIEND":
      return Object.assign({}, state, {
        friend: action.friend,
        wasSeletedFriend:action.wasSeletedFriend
      })
     break;
    case "CHAT_RECEIVE_MESSAGE_FROM_FRIEND":
    case "CHAT_SEND_MESSAGE_TO_FRIEND":
    case "CHAT_ON_FRIENDS_TYPPING_MESSAGE" : 
    case "CHAT_GET_FRIENDS":
      return Object.assign({}, state, {
        friends: action.friends,
      })
    break;
    default:
      return state
  }
}