export interface IChatState  {
  friends?:  Array<Object>,
  currentMessage?:string,
  currentChatUserId?:Number
  currentChatUserName?:string,
  messages:Array<Object>,
  wasSeletedFriend?:Boolean,
  isMonted?:Boolean
}

