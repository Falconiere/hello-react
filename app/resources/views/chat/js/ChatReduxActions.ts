
/**
 * Interface
 */
import {IPromise} from "../../../helpers/InterfaceHelper"



/**
 * The chatSelectedFriend is responsable by to change
 * state when a friend is selected
 * @param {Object} friend 
 * @returns {Object}
 */
export function chatSelectedFriend(friend:any){
  return {
    type: "CHAT_SELECTED_FRIEND",
    friend: friend,
    wasSeletedFriend:true
  }
}


/**
 * The  chatGetFriends is responsa
 * 
 * @param {Object} friends 
 * @param {Object} payload 
 * @returns {Object}
 */
export function chatGetFriends(friends:any,payload:IPromise,callBack:Function){
  return ((dispatch:any)=> {
      payload
      .then((success:any)=>{
        let friends = success.data.data.data;
        callBack(friends)
        dispatch({type : 'CHAT_GET_FRIENDS', friends : friends, isMonted:true})
      })
      .catch((error:any)=>{
        dispatch({type : 'CHAT_GET_FRIENDS_ERROR', friends : friends, error:error})
      });
  })
}

/**
 * The chatReceiveMessageFromFriend is resonsable by to receive the message from friend
 * 
 * @param {Object} friend 
 * @param {Object} message 
 * @returns {Object}
 */
export function chatReceiveMessageFromFriend(friends:any,messageObj:any){

  let message = typeof messageObj==="object" ? messageObj : JSON.parse(messageObj)

  friends.forEach((friend:any) => {
    console.log("ppppppppp",message);
    if(parseInt(friend.id)==parseInt(message.from)){
      
        if(friend.hasOwnProperty("messages")){
        
          friend.messages.push(message)
        }else{
          friend["messages"] = [message]
        }
    }
  })
  return  {
    type: "CHAT_RECEIVE_MESSAGE_FROM_FRIEND",
    friends: friends
  };
}
/**
 * The chatSendMessageToFriend is resonsable by to send the message to friend
 * 
 * @param {Object} friend 
 * @param {Object} message 
 * @returns {Object}
 */
export function chatSendMessageToFriend(friends:any,message:any){
  friends.forEach((friend:any) => {
      if(parseInt(friend.id)===parseInt(message.to)){
          if(friend.hasOwnProperty("messages")){
            friend.messages.push(message)
          }else{
            friend["messages"] = [message]
          }
      }
  })
  return  {
    type: "CHAT_SEND_MESSAGE_TO_FRIEND",
    friends:friends
  };
}

/**
 * The chatOnFriendsTypingMessage is responsable by to change the state when the
 * user is typing messages
 * 
 * @param {Object} friends 
 * @param {Object} friendsTyping 
 * @returns {Object}
 */
export function chatOnFriendTypingMessage(friends:any,friendTyping:any,isTyping:Boolean){
  friends.forEach((friend:any) => {
    if(parseInt(friend.id)===parseInt(friendTyping.id)){
      friend.isTyping = isTyping;
      //console.log(friendTyping)
      console.log(isTyping)
    }
  });
  return  {
    type: "CHAT_ON_FRIENDS_TYPPING_MESSAGE",
    friends:friends
  };
}




