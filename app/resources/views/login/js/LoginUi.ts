/**
 * Dependencias
 */
import * as React from "react"

import UserControllerApi from "../../../controllers/UserControllerApi"

export default class LoginUi extends React.Component<any>{

  public state = {
    email:"",
    password:""
  }
  constructor(props:any){
    super(props)
    this.onSubmitForm = this.onSubmitForm.bind(this)
    this.state = this.state

    let token = localStorage.getItem("api_token");
    if(token && token!=null && token!="null"){
      localStorage.setItem("api_token",null);
    }
  }

  public onSubmitForm(event:any) : void{
      let self = this
      let elements = event.target.elements;
      
      let body :any = {}
      for(let i = 0 ; i < elements.length ; i++){
        let item = elements.item(i);

        if(item.name=="password"){
          body[item.name] = item.value
          self.setState({password:item.value})
        }
        if(item.name=="email"){
          body[item.name] = item.value
          self.setState({email:item.value})
        }
      }

      if(Object.keys(body).length > 1){
        UserControllerApi.loginUser(body)
        .then((success:any)=>{
          if(success.status=="ok"){
            let data =success.data.data
            let token = data.token
            
            localStorage.setItem("api_token",token)
            localStorage.setItem("user_data",JSON.stringify(data))
            
            self.props.history.push(`/`, self.state )
          }
        })
        .catch((error:any)=>{
          console.log(error)
        })
      }
      event.preventDefault();
  }
}