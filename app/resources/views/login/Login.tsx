/**
 * Dependences
 */
import * as React from "react"


/**
 * Controllers
 */
import LoginUi from "./js/LoginUi"

class Login extends LoginUi{

  public render() : Object{
    return(
      <div className="login container">
        <form className="form-signin" onSubmit={this.onSubmitForm}>
          <h2 className="form-signin-heading">Please sign in</h2>
            <label  className="sr-only">Email address</label>
              <input type="email" className="form-control" name="email" placeholder="Email address" />
            <label className="sr-only">Password</label>
            <input type="password"  name="password"className="form-control" placeholder="Password" />
            <div className="checkbox">
              <label>
                <input type="checkbox" value="remember-me" /> Remember me
              </label>
            </div>
          <button className="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        </form>
      </div>
    )
  }
}
export default Login
