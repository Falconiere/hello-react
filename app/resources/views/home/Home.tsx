/**
 * Dependences
 */
import * as React from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux'



/**
 * Controller from view
 */
import HomeUi from './js/HomeUi'
class Home extends HomeUi {

  render(){
    return (
      <div className="home">
      	<h1>Home</h1>
      </div>
    )
  }
}
export default withRouter(connect()(Home) as any)

