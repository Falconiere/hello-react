import * as React from 'react';

export default class AppBaseUi extends React.Component<any,any,any> {
	
  state : any
	public constructor(props:any) {
    super(props);
      this.state = { error: null };

  }

  /**
   * The componentDidMount is a native function from react-component 
   * @return {null} [description]
  */
  public componentDidMount() {
    
  }
  /**
   * The componentWillUnmount is a native function from react-component 
   * @return {null} [description]
  */
  public componentWillUnmount() {
   
  }
}
