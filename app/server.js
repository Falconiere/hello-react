const express = require('express');
const bodyParser = require('body-parser');
const app = express();

// Define the port to run on
app.set('port', 3000);

//se the root server
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.static(__dirname + '/public'));
app.set('views', __dirname + '/resources/views');
app.set('view engine', 'ejs');



/*
  Load the SPA
 */
app.get("/*",function(req,res){
  res.render('root/index');
});

// Start server and listen  requests
var server = app.listen(app.get('port'), function() {
  var port = server.address().port;
  console.log('The admin run on: http://localhost:' + port);
});
